package com.example.imc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button novoS = (Button) findViewById(R.id.button);

        novoS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ImageView img = findViewById(R.id.imageView);

                img.setImageResource(R.drawable.perfil);

                EditText peso1 = findViewById(R.id.editText);
                EditText altura1 = findViewById(R.id.editText2) ;

                String peso2 = peso1.getText().toString();
                String altura2 = altura1.getText().toString();

                float peso3 = Float.parseFloat(peso2);
                float altura3 = Float.parseFloat(altura2);

                float resultado =  peso3/(altura3*altura3);

                String resultadoFinal = null;

                if(resultado < 18.5){

                    resultadoFinal = "Magro";
                    img.setImageResource(R.drawable.abaixopeso);

                }else if (resultado >= 18.5 && resultado <= 24.9){

                    resultadoFinal = "Normal";
                    img.setImageResource(R.drawable.normal);

                }else if (resultado >= 25.0 && resultado <= 29.9){

                    resultadoFinal = "SobrePeso";
                    img.setImageResource(R.drawable.sobrepeso);

                }else if (resultado >= 30.0 && resultado <= 34.9){

                    resultadoFinal = "Obesidade";
                    img.setImageResource(R.drawable.obesidade1);

                }else if (resultado >= 35.0 && resultado <= 39.9){

                    resultadoFinal = "Obesidade";
                    img.setImageResource(R.drawable.obesidade2);

                }else if( resultado >= 40.0){
                    resultadoFinal = "Obesidade";
                    img.setImageResource(R.drawable.obesidade3);
                }
                TextView tv = (TextView) findViewById(R.id.textView);
                tv.setText(resultadoFinal);

            }
        });

    }
}
